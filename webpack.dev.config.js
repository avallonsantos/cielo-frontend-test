const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')

module.exports = {
    entry: './src/app/index.js',
    output: {
        filename: 'app.bundle.js',
        path: path.resolve(__dirname, './dist/'),
        publicPath: ''
    },
    mode: 'development',
    devServer: {
        contentBase: path.resolve(__dirname, 'dist'),
        index: 'index.html',
        port: 9000,
    },
    module: {
        rules: [
            {
                test: /\.css$/,
                use: [
                    'style-loader',
                    'css-loader'
                ]
            },
            {
              test: /\.scss$/,
              use: [
                MiniCssExtractPlugin.loader,
                'css-loader',
                'sass-loader'
              ]
            },
        ]
    },
    plugins: [
      new MiniCssExtractPlugin({
        filename: 'app.css'
      }),
      new HtmlWebpackPlugin({
          template: './src/index.html'
      })
    ]
}
