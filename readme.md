# Teste Cielo - Requisições

Aplicação básica para um blog fake retornando posts de uma API fake - [JSONPlaceholder](https://jsonplaceholder.typicode.com/). Trata-se de uma aplicação que, apesar de simples, é efetiva.

## Instalação

Para rodar a aplicação, basta clonar o repositório, e, em seguida, rodar os comandos:

```bash
npm install
```

Logo após a instalação das dependências necessárias, rodar:

```bash
npm run dev
```

O último comando abrirá um servidor de testes na porta 9000 para verificação do teste. Caso o navegador não abra automaticamente, por favor, abra-o manualmente e digite: http://localhost:9000 na barra de navegação;

É importante ressaltar que há a necessidade de ter o node instalado. Caso não possua, basta acessar [nodejs](https://nodejs.org/en/), fazer o download e instalá-lo seguindo o passo-a-passo do instalador.

## Dependências

O projeto utiliza webpack para gerenciamento das dependências de produção.

Para gerar os arquivos de produção, basta rodar o comando:

```bash
npm run build
```

## Resultado

A aplicação retorna uma promise que apresenta os posts ou retorna uma mensagem de erro.
