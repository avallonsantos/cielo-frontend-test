import angular from 'angular'
import '../../node_modules/bootstrap-4-grid/scss/grid.scss'
import '../app/assets/app.scss'

const app = angular.module('cieloApp', [])

function GetPostsController($scope, $http) {
  $scope.loading = true
  let postsPromise = $http.get('https://jsonplaceholder.typicode.com/posts?_limit=9')

  const success = (response) => {
    console.log('Sucesso')
    $scope.posts = response.data
  }

  const error = (response) => {
    $scope.error = true
  }

  postsPromise.then(success, error)

  $scope.loading = false

}

GetPostsController.$inject = ["$scope", "$http"];
app.controller('PostsController', GetPostsController)
